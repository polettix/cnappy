#!/usr/bin/env perl
use Mojolicious::Lite -signatures;
use POSIX 'strftime';
get '/' => sub ($c) { $c->render(template => "index") };
get '/json' => sub ($c) {
   $c->render(json => {now => strftime('%Y%m%dT%H%M%SZ', gmtime)});
};
get '/date' => sub ($c) {
   $c->res->headers->content_type('text/plain');
   $c->render(text => gmtime . " (UTC)\n");
};
app->start;
__DATA__
@@ index.html.ep
<!DOCTYPE html>
<html>
  <head><title>Hello, world!</title></head>
  <body><h1>Hello, world!</h1></body>
</html>
