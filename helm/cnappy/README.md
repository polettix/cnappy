# cnappy - Helm chart

This is an [Helm][] chart to deploy the `cnappy` [Docker][] image in
[Kubernetes][]. It has been tested with Helm version 2 but it's expected to
work fine with Helm version 3 as well.

The contents of this Helm chart are licensed according to the Apache
License 2.0 (see file `LICENSE` in the chart's root directory):

>  Copyright 2020 by Flavio Poletti
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.

[Helm]: https://helm.sh/
[Docker]:  https://www.docker.com/
[Kubernetes]: https://kubernetes.io/
