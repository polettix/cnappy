# cnappy - Docker image

This is a [Docker][] image for `cnappy`. It implements a basic web server
that listens on port 8080 (inside the container) and supports the
following endpoints:

- `/`: prints out a greeting message (`text/html;charset=utf-8`)
- `/json`: returns an `application/json` ormatted string, with some
  randomness inside
- `/date`: returns the current date as `plain/text`

Sub-directory `helm` contains a [Helm][] chart to deploy the application's
image in [Kubernetes][].

## Building

To build this image, [dibs][] is needed. [Getting is is easy][]. Go in the
`docker` directory (where this `README.md` file is) and run:

```shell
# first time you will have to explicitly build the "base" images
dibs bases target

# from now on, if you want to rebuild...
dibs target

# if you didn't change the prerequisites...
dibs quick
```

## Running

If you *run* the image as-is you get a little help:

```
$ docker run --rm registry.gitlab.com/polettix/cnappy:0.3

*** WARNING: not remapping user <urist> to user id 0

Sub-commands: daemon, chart
```

Ignore the warnings, it's not affecting anything in this case.

Sub-command `daemon` lets you start the web server.

Sub-command `chart` prints out on standard output a gzipped-tar archive of the
Helm chart, so that you can get both the image and the associated chart in one
single pass.

```
$ docker run --rm registry.gitlab.com/polettix/cnappy:0.3 chart > cnappy.tar.gz
```


The contents of this repository are licensed according to the Apache
License 2.0 (see file `LICENSE` in the project's root directory):

>  Copyright 2020 by Flavio Poletti
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.

[Docker]: https://www.docker.com/
[dibs]: https://github.com/polettix/dibs
[Getting it is easy]: https://blog.polettix.it/hi-from-dibs/#installing-dibs
[Kubernetes]: https://kubernetes.io/
[Helm]: https://helm.sh/
